terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.65.0"
    }
  }
  # details er injected by GitLab
  backend "http" {
  }
}

provider "azurerm" {
  features {}
}

# test resources
resource "azurerm_resource_group" "example" {
  name     = "example-rg"
   location = "westeurope"
}

resource "azurerm_resource_group" "example-denied" {
  name     = "example-denied-rg"
  location = "northeurope"
}
